# Fractal Curves

This repository is a collection of fractal curves (Cesaro curves and
Dragon curves) preserved from a now retired (circa 2009) project for
generating these kinds of curves.

Each curve directory has a selection of curves in PDF format and in 
various PNG image sizes. Each curve also has a small amount of
original metadata in a matching YAML file.


## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  <img alt="Creative Commons License" 
       style="border-width:0" 
       src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png"/>
</a>

These works are licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  Creative Commons Attribution-ShareAlike 4.0 International License
</a>.
